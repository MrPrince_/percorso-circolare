﻿using AutoMapper;
using PercorsoCircolare.DAL;
using PercorsoCircolare.DTO;

namespace PercorsoCircolare.Manager
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Resources, ResourceDTO>()
               .ForMember(r => r.Id, map => map.MapFrom(m => m.Id))
               .ForMember(r => r.Username, map => map.MapFrom(m => m.Username))
               .ForMember(r => r.Name, map => map.MapFrom(m => m.Name))
               .ForMember(r => r.Surname, map => map.MapFrom(m => m.Surname))
               .ForMember(r => r.IdStatus, map => map.MapFrom(m => m.IdStatus));

            CreateMap<ResourceDTO, Resources>()
               .ForMember(r => r.Id, map => map.MapFrom(m => m.Id))
               .ForMember(r => r.Username, map => map.MapFrom(m => m.Username))
               .ForMember(r => r.Name, map => map.MapFrom(m => m.Name))
               .ForMember(r => r.Surname, map => map.MapFrom(m => m.Surname))
               .ForMember(r => r.Lessons, map => map.Ignore())
               .ForMember(r => r.Lessons1, map => map.Ignore())
               .ForMember(r => r.Courses, map => map.Ignore())
               .ForMember(r => r.IdStatus, map => map.MapFrom(m => m.IdStatus));

            CreateMap<Courses, CourseDTO>()
               .ForMember(r => r.Id, map => map.MapFrom(m => m.Id))
               .ForMember(r => r.Description, map => map.MapFrom(m => m.Description))
               .ForMember(r => r.Year, map => map.MapFrom(m => m.Year))
               .ForMember(r => r.StartDate, map => map.MapFrom(m => m.StartDate))
               .ForMember(r => r.EndDate, map => map.MapFrom(m => m.EndDate))
               .ForMember(r => r.IdResourceCoordinator, map => map.MapFrom(m => m.IdResourceCoordinator)).ReverseMap();

            CreateMap<Modules, ModuleDTO>()
               .ForMember(r => r.Id, map => map.MapFrom(m => m.Id))
               .ForMember(r => r.Name, map => map.MapFrom(m => m.Name))
               .ForMember(r => r.Credits, map => map.MapFrom(m => m.Credits)).ReverseMap();

            CreateMap<Lessons, LessonDTO>()
               .ForMember(r => r.Id, map => map.MapFrom(m => m.Id))
               .ForMember(r => r.IdCourse, map => map.MapFrom(m => m.IdCourse))
               .ForMember(r => r.IdModule, map => map.MapFrom(m => m.IdModule))
               .ForMember(r => r.Description, map => map.MapFrom(m => m.Description))
               .ForMember(r => r.Date, map => map.MapFrom(m => m.Date))
               .ForMember(r => r.Room, map => map.MapFrom(m => m.Room))
               .ForMember(r => r.IdTeacher, map => map.MapFrom(m => m.IdTeacher))
               .ForMember(r => r.IdTeacherBackup, map => map.MapFrom(m => m.IdTeacherBackup)).ReverseMap();
        }
    }
}