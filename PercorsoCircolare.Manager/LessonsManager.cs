﻿using AutoMapper;
using PercorsoCircolare.DAL;
using PercorsoCircolare.DTO;
using System.Collections.Generic;
using System.Linq;

namespace PercorsoCircolare.Manager
{
    public static class LessonsManager
    {
        /// <summary>
        /// Metodo per recuperare tutte le lezioni
        /// </summary>
        public static List<LessonDTO> GetAll()
        {
            using (PercorsoCircolareEntities dc = new PercorsoCircolareEntities())
            {
                var values = dc.Lessons.ToList();
                List<LessonDTO> lessons = Mapper.Map<List<Lessons>, List<LessonDTO>>(values);
                return lessons;
            }
        }

        /// <summary>
        /// Metodo per recuperare informazioni di una lezione partendo dall'id
        /// </summary>
        /// <param name="id">Id della lezione</param>
        public static LessonDTO GetById(int id)
        {
            using (PercorsoCircolareEntities dc = new PercorsoCircolareEntities())
            {
                var value = dc.Lessons.Where(l => l.Id == id).FirstOrDefault();
                LessonDTO lesson = Mapper.Map<Lessons, LessonDTO>(value);
                return lesson;
            }
        }

        /// <summary>
        /// Metodo per l'aggiunta a database di una nuova lezione
        /// </summary>
        /// <param name="lesson">Nuova lezione</param>
        public static void CreateLesson(LessonDTO lesson)
        {
            using (PercorsoCircolareEntities dc = new PercorsoCircolareEntities())
            {
                Lessons newLesson = Mapper.Map<LessonDTO, Lessons>(lesson);
                dc.Lessons.Add(newLesson);
                dc.SaveChanges();
                ModulesManager.UpdateModuleCredits(newLesson.IdModule);
            }
        }

        /// <summary>
        /// Metodo per l'eliminazione da database di una lezione
        /// </summary>
        /// <param name="id">Id della lezione</param>
        public static void DeleteById(int id)
        {
            using (PercorsoCircolareEntities dc = new PercorsoCircolareEntities())
            {
                var value = dc.Lessons.Where(l => l.Id == id).FirstOrDefault();
                Modules module = dc.Modules.Where(m => m.Id == value.IdModule).FirstOrDefault();
                dc.Lessons.Remove(value);
                dc.SaveChanges();
                ModulesManager.UpdateModuleCredits(module.Id);
            }
        }
    }
}