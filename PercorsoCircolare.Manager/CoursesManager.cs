﻿using AutoMapper;
using PercorsoCircolare.DAL;
using PercorsoCircolare.DTO;
using System.Collections.Generic;
using System.Linq;

namespace PercorsoCircolare.Manager
{
    public static class CoursesManager
    {
        /// <summary>
        /// Metodo per recuperare tutti i percorsi
        /// </summary>
        public static List<CourseDTO> GetAll()
        {
            using (PercorsoCircolareEntities dc = new PercorsoCircolareEntities())
            {
                var values = dc.Courses.ToList();
                List<CourseDTO> courses = Mapper.Map<List<Courses>, List<CourseDTO>>(values);
                return courses;
            }
        }

        /// <summary>
        /// Metodo per recuperare informazioni di un percorso partendo dall'id
        /// </summary>
        /// <param name="id">Id del percorso</param>
        public static CourseDTO GetById(int id)
        {
            using (PercorsoCircolareEntities dc = new PercorsoCircolareEntities())
            {
                var value = dc.Courses.Where(c => c.Id == id).FirstOrDefault();
                CourseDTO course = Mapper.Map<Courses, CourseDTO>(value);
                return course;
            }
        }

        /// <summary>
        /// Metodo per l'aggiunta a database di un nuovo percorso
        /// </summary>
        /// <param name="course">Nuovo percorso</param>
        public static void CreateCourse(CourseDTO course)
        {
            using (PercorsoCircolareEntities dc = new PercorsoCircolareEntities())
            {
                Courses newCourse = Mapper.Map<CourseDTO, Courses>(course);
                dc.Courses.Add(newCourse);
                dc.SaveChanges();
            }
        }
    }
}