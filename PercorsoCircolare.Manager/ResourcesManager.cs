﻿using AutoMapper;
using PercorsoCircolare.DAL;
using PercorsoCircolare.DTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PercorsoCircolare.Manager
{
    public static class ResourcesManager
    {
        /// <summary>
        /// Metodo per recuperare tutte le risorse
        /// </summary>
        public static List<ResourceDTO> GetAll()
        {
            using (PercorsoCircolareEntities dc = new PercorsoCircolareEntities())
            {
                var values = dc.Resources.ToList();
                List<ResourceDTO> resources = Mapper.Map<List<Resources>, List<ResourceDTO>>(values);
                return resources;
            }
        }

        /// <summary>
        /// Metodo per recuperare informazioni di una risorsa partendo dall'id
        /// </summary>
        /// <param name="id">Id della risorsa</param>
        public static ResourceDTO GetById(int id)
        {
            using (PercorsoCircolareEntities dc = new PercorsoCircolareEntities())
            {
                var value = dc.Resources.Where(r => r.Id == id).FirstOrDefault();
                ResourceDTO resource = Mapper.Map<Resources, ResourceDTO>(value);
                return resource;
            }
        }

        /// <summary>
        /// Metodo per l'aggiunta a database di una nuova risorsa
        /// </summary>
        /// <param name="resource">Nuova risorsa</param>
        public static void CreateResource(ResourceDTO resource)
        {
            using (PercorsoCircolareEntities dc = new PercorsoCircolareEntities())
            {
                Resources newResource = Mapper.Map<ResourceDTO, Resources>(resource);

                //Controllo validità id inserito
                if (dc.Resources.FirstOrDefault(r => r.Id == resource.Id) != null)
                {
                    throw new Exception("Id già presente nel database!");
                }
                //controllo validità username inserito
                else if (dc.Resources.FirstOrDefault(r => r.Username == resource.Username) != null)
                {
                    throw new Exception("Username già presente nel database!");
                }
                dc.Resources.Add(newResource);
                dc.SaveChanges();
            }
        }
    }
}