﻿using AutoMapper;
using PercorsoCircolare.DAL;
using PercorsoCircolare.DTO;
using System.Collections.Generic;
using System.Linq;

namespace PercorsoCircolare.Manager
{
    public static class ModulesManager
    {
        /// <summary>
        /// Metodo per recuperare tutti i moduli
        /// </summary>
        public static List<ModuleDTO> GetAll()
        {
            using (PercorsoCircolareEntities dc = new PercorsoCircolareEntities())
            {
                var values = dc.Modules.ToList();
                List<ModuleDTO> modules = Mapper.Map<List<Modules>, List<ModuleDTO>>(values);
                return modules;
            }
        }

        /// <summary>
        /// Metodo per recuperare informazioni di un modulo partendo dall'id
        /// </summary>
        /// <param name="id">Id del modulo</param>
        public static ModuleDTO GetById(int id)
        {
            using (PercorsoCircolareEntities dc = new PercorsoCircolareEntities())
            {
                var value = dc.Modules.Where(m => m.Id == id).FirstOrDefault();
                ModuleDTO module = Mapper.Map<Modules, ModuleDTO>(value);
                return module;
            }
        }

        /// <summary>
        /// Metodo per l'aggiunta a database di un nuovo modulo
        /// </summary>
        /// <param name="module">Nuovo modulo</param>
        public static void CreateModule(ModuleDTO module)
        {
            using (PercorsoCircolareEntities dc = new PercorsoCircolareEntities())
            {
                Modules newModule = Mapper.Map<ModuleDTO, Modules>(module);
                dc.Modules.Add(newModule);
                dc.SaveChanges();
            }
        }

        /// <summary>
        /// Metodo per l'aggiornamento del valore in crediti di un modulo
        /// </summary>
        /// <param name="moduleId">Id del modulo da aggiornare</param>
        public static void UpdateModuleCredits(int moduleId)
        {
            using (PercorsoCircolareEntities dc = new PercorsoCircolareEntities())
            {
                Modules module = dc.Modules.Where(m => m.Id == moduleId).FirstOrDefault();
                int lessons = dc.Lessons.Where(l => l.IdModule == module.Id).Count();
                module.Credits = lessons * 10;
                dc.SaveChanges();
            }
        }
    }
}