﻿using System;

namespace PercorsoCircolare.DTO
{
    public class CourseDTO
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public int Year { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int IdResourceCoordinator { get; set; }
    }
}