﻿using System;

namespace PercorsoCircolare.DTO
{
    public class LessonDTO
    {
        public int Id { get; set; }
        public int IdModule { get; set; }
        public int IdCourse { get; set; }
        public string Description { get; set; }
        public DateTime Date { get; set; }
        public string Room { get; set; }
        public int IdTeacher { get; set; }
        public int IdTeacherBackup { get; set; }
    }
}