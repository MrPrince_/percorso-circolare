﻿namespace PercorsoCircolare.DTO
{
    public class ModuleDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Credits { get; set; }
    }
}