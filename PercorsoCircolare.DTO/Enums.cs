﻿namespace PercorsoCircolare.DTO
{
    public enum eResourceStatus
    {
        AVAILABLE = 0,
        NOT_AVAILABLE = 1
    }
}