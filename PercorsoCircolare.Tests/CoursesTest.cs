﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PercorsoCircolare.Manager;

namespace PercorsoCircolare.Tests
{
    [TestClass]
    public class CoursesTest
    {
        [TestMethod]
        public void GetAll()
        {
            var ret = CoursesManager.GetAll();
            Assert.IsTrue(ret != null);
            Assert.IsTrue(ret.Count >= 0);
        }

        [TestMethod]
        public void GetById()
        {
            var courses = CoursesManager.GetAll();
            int id = courses[0].Id;
            var ret = CoursesManager.GetById(id);
            Assert.IsTrue(ret != null);
        }
    }
}