﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PercorsoCircolare.Manager;

namespace PercorsoCircolare.Tests
{
    [TestClass]
    public class ResourcesTest
    {
        [AssemblyInitialize]
        public static void Setup(TestContext context)
        {
            Mapper.Initialize(x =>
            {
                x.AddProfile<MappingProfile>();
            });

            Mapper.Configuration.AssertConfigurationIsValid();
        }

        [TestMethod]
        public void GetAll()
        {
            var ret = ResourcesManager.GetAll();
            Assert.IsTrue(ret != null);
            Assert.IsTrue(ret.Count >= 0);
        }

        [TestMethod]
        public void GetById()
        {
            var res = ResourcesManager.GetAll();
            int id = res[0].Id;
            var ret = ResourcesManager.GetById(id);
            Assert.IsTrue(ret != null);
        }
    }
}
