﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PercorsoCircolare.Manager;

namespace PercorsoCircolare.Tests
{
    [TestClass]
    public class LessonsTest
    {
        [TestMethod]
        public void GetAll()
        {
            var ret = LessonsManager.GetAll();
            Assert.IsTrue(ret != null);
            Assert.IsTrue(ret.Count >= 0);
        }

        [TestMethod]
        public void GetById()
        {
            var less = LessonsManager.GetAll();
            int id = less[0].Id;
            var ret = LessonsManager.GetById(id);
            Assert.IsTrue(ret != null);
        }
    }
}