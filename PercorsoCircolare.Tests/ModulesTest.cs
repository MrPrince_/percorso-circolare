﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PercorsoCircolare.Manager;

namespace PercorsoCircolare.Tests
{
    [TestClass]
    public class ModulesTest
    {
        [TestMethod]
        public void GetAll()
        {
            var ret = ModulesManager.GetAll();
            Assert.IsTrue(ret != null);
            Assert.IsTrue(ret.Count >= 0);
        }

        [TestMethod]
        public void GetById()
        {
            var mods = ModulesManager.GetAll();
            int id = mods[0].Id;
            var ret = ModulesManager.GetById(id);
            Assert.IsTrue(ret != null);
        }
    }
}