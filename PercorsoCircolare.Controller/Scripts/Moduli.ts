import { Modulo } from "../DTO/Modulo";
import { URLHelper } from "../Helper/URLHelper";
import { ToastHelper } from "../Helper/ToastHelper";
import { ToastTypes } from "../Enums";
import { ErrorHelper } from "../Helper/ErrorHelper";

$(document).ready(function () {

    //inizializzazione form creazione modulo
    (<any>$("#form")).validate({
        rules: {
            name: {
                required: true
            }
        },
        messages: {
            name: "Campo obbligatorio"
        },
        submitHandler: function (form) {
            //recupero informazioni inserite nel form
            var value = new Modulo();
            value.Name = form['name'].value as string;
            value.Credits = 0;

            //chiamata per creazione modulo a database
            $.ajax({
                type: "POST",
                dataType: 'text',
                url: URLHelper.getMainWebApiUrl() + 'api/Modules/CreateModule',
                data: value,
                crossDomain: false,
                xhrFields: {
                    withCredentials: false
                },
                success: function (data) {
                    //reset campi del form
                    form['name'].value = null;
                    ToastHelper.Toast(ToastTypes.Success, "Nuovo modulo creato correttamente, ricaricare la pagina per visualizzarlo");
                },
                error: function (xhr, textStatus) {
                    ErrorHelper.Handle(xhr, textStatus);
                }
            });
        }
    });

    //chiamata per recuperare tutti i moduli
    $.ajax({
        type: "GET",
        dataType: 'text',
        url: URLHelper.getMainWebApiUrl() + 'api/Modules/GetAll',
        crossDomain: false,
        xhrFields: {
            withCredentials: false
        },
        success: function (data) {
            //aggiunta dei moduli all'interno della tabella
            let json: Array<Modulo> = $.parseJSON(data);
            for (let r of json) {
                $('#tableBody').append('<tr>'
                    + '<td><a href="DettaglioModulo.html?id=' + r.Id.toString() + '">' + r.Id.toString() + '</a></td>'
                    + '<td>' + r.Name + '</td>'
                    + '<td>' + r.Credits + '</td>'
                    + '</tr>');
            }
        },
        error: function (xhr, textStatus) {
            ErrorHelper.Handle(xhr, textStatus);
        }
    });
});

//funzione di ricerca valori all'interno della tabella dei moduli
$("#filterText").keyup(function () {
    var rows = $("#tableBody").find("tr").hide();
    let val = $("#filterText").val() as string;
    if (val != "") {
        var data = val.split(" ");
        $.each(data, function (i, v) {
            if (v != "") {
                rows.filter(":contains('" + v + "')").show();
            }
        });
    }
    else {
        rows.show();
    }
});