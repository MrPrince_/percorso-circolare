"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var URLHelper_1 = require("../Helper/URLHelper");
var ErrorHelper_1 = require("../Helper/ErrorHelper");
$(document).ready(function () {
    var queryParamId = URLHelper_1.URLHelper.getUrlParameter("id");
    //chiamata per recupero informazioni dettaglio percorso
    $.ajax({
        type: "GET",
        dataType: 'text',
        url: URLHelper_1.URLHelper.getMainWebApiUrl() + 'api/Courses/GetById/' + queryParamId,
        crossDomain: false,
        xhrFields: {
            withCredentials: false
        },
        success: function (data) {
            var options = { year: 'numeric', month: 'numeric', day: 'numeric' };
            var res = $.parseJSON(data);
            var start = new Date(res.StartDate);
            var end = new Date(res.EndDate);
            $('#courseId').append(res.Id.toString());
            $('#courseDescription').append(res.Description.toString());
            $('#courseYear').append(res.Year.toString());
            $('#courseStartDate').append(start.toLocaleDateString('it-IT', options));
            $('#courseEndDate').append(end.toLocaleDateString('it-IT', options));
            $('#courseIdResourceCoordinator').append('<a href="DettaglioRisorsa.html?id=' + res.IdResourceCoordinator + '">' + res.IdResourceCoordinator.toString() + '</a>');
        },
        error: function (xhr, textStatus) {
            ErrorHelper_1.ErrorHelper.Handle(xhr, textStatus);
        }
    });
});
