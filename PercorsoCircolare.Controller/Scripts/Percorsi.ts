import { Percorso } from "../DTO/Percorso";
import { Risorsa } from "../DTO/Risorsa";
import { URLHelper } from "../Helper/URLHelper";
import { ToastHelper } from "../Helper/ToastHelper";
import { ToastTypes } from "../Enums";
import { ErrorHelper } from "../Helper/ErrorHelper";

$(document).ready(function () {

    //inizializzazione form creazione percorso
    (<any>$("#form")).validate({
        rules: {
            description: {
                required: true
            },
            year: {
                required: true
            },
            startDate: {
                required: true
            },
            endDate: {
                required: true
            },
            coordinators: {
                required: true
            }
        },
        messages: {
            description: "Campo obbligatorio",
            year: "Campo obbligatorio",
            startDate: "Campo obbligatorio",
            endDate: "Campo obbligatorio",
            coordinators: "Campo obbligatorio"
        },
        submitHandler: function (form) {
            //recupero informazioni inserite nel form
            var value = new Percorso();
            value.Description = form['description'].value as string;
            value.Year = form['year'].value as number;
            value.StartDate = form['startDate'].value as Date;
            value.EndDate = form['endDate'].value as Date;
            let x = form['coordinators'].value as string;
            value.IdResourceCoordinator = parseInt(x.split(' ')[0]);

            //chiamata per creazione percorso a database
            $.ajax({
                type: "POST",
                dataType: 'text',
                url: URLHelper.getMainWebApiUrl() + 'api/Courses/CreateCourse',
                data: value,
                crossDomain: false,
                xhrFields: {
                    withCredentials: false
                },
                success: function (data) {
                    //reset campi del form
                    form['description'].value = null;
                    form['year'].value = null;
                    form['startDate'].value = null;
                    form['endDate'].value = null;
                    form['coordinators'].value = null;
                    ToastHelper.Toast(ToastTypes.Success, "Nuovo percorso creato correttamente, ricaricare la pagina per visualizzarlo")
                },
                error: function (xhr, textStatus) {
                    ErrorHelper.Handle(xhr, textStatus);
                }
            });
        }
    });

    //chiamata per recuperare tutti i percorsi
    $.ajax({
        type: "GET",
        dataType: 'text',
        url: URLHelper.getMainWebApiUrl() + 'api/Courses/GetAll',
        crossDomain: false,
        xhrFields: {
            withCredentials: false
        },
        success: function (data) {
            //aggiunta dei percorsi all'interno della tabella
            let json: Array<Percorso> = $.parseJSON(data);
            var options = { year: 'numeric', month: 'numeric', day: 'numeric' };
            for (let r of json) {
                var start = new Date(<any>r.StartDate);
                var end = new Date(<any>r.EndDate);
                $('#tableBody').append('<tr>'
                + '<td><a href="DettaglioPercorso.html?id=' + r.Id.toString() + '">' + r.Id.toString() + '</a></td>'
                + '<td>' + r.Description + '</td>'
                + '<td>' + r.Year + '</td>'
                + '<td>' + start.toLocaleDateString('it-IT', options) + '</td>'
                + '<td>' + end.toLocaleDateString('it-IT', options) + '</td>'
                + '</tr>');
            }
        },
        error: function (xhr, textStatus) {
            ErrorHelper.Handle(xhr, textStatus);
        }
    });

    //chiamata per recuperare le risorsa da inserire all'interno dell'oggetto datalist
    $.ajax({
        type: "GET",
        dataType: 'text',
        url: URLHelper.getMainWebApiUrl() + 'api/Resources/GetAll',
        crossDomain: false,
        xhrFields: {
            withCredentials: false
        },
        success: function (data) {
            //aggiunta delle singole risorse al campo option della datalist
            let json: Array<Risorsa> = $.parseJSON(data);
            for (let r of json) {
                $('#coordinators').append('<option value="' + r.Id + ' - ' + r.Surname + ' ' + r.Name + '">');
            }
        },
        error: function (xhr, textStatus) {
            ErrorHelper.Handle(xhr, textStatus);
        }
    });
});

//funzione di ricerca valori all'interno della tabella dei percorsi
$("#filterText").keyup(function () {
    var rows = $("#tableBody").find("tr").hide();
    let val = $("#filterText").val() as string;
    if (val != "") {
        var data = val.split(" ");
        $.each(data, function (i, v) {
            if(v != ""){
                rows.filter(":contains('" + v + "')").show();
            }
        });
    }
    else {
        rows.show();
    }
});