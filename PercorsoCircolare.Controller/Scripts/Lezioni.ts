import { Lezione } from "../DTO/Lezione";
import { Risorsa } from "../DTO/Risorsa";
import { Modulo } from "../DTO/Modulo";
import { Percorso } from "../DTO/Percorso";
import { URLHelper } from "../Helper/URLHelper";
import { ToastHelper } from "../Helper/ToastHelper";
import { ToastTypes } from "../Enums";
import { ErrorHelper } from "../Helper/ErrorHelper";

$(document).ready(function () {

    //inizializzazione form creazione lezione
    (<any>$("#form")).validate({
        rules: {
            courses: {
                required: true
            },
            modules: {
                required: true
            },
            description: {
                required: true
            },
            date: {
                required: true
            },
            room: {
                required: true
            },
            teachers: {
                required: true
            },
            teachersBackup: {
                required: true
            },
        },
        messages: {
            courses: "Campo obbligatorio",
            modules: "Campo obbligatorio",
            description: "Campo obbligatorio",
            date: "Campo obbligatorio",
            room: "Campo obbligatorio",
            teachers: "Campo obbligatorio",
            teachersBackup: "Campo obbligatorio"
        },
        submitHandler: function (form) {
            //recupero informazioni inserite nel form
            var value = new Lezione();
            let course = form['courses'].value as string;
            value.IdCourse = parseInt(course.split(' ')[0]);
            let mod = form['modules'].value as string;
            value.IdModule = parseInt(mod.split(' ')[0]);
            value.Description = form['description'].value as string;
            value.Date = form['date'].value as Date;
            value.Room = form['room'].value as string;
            let teacher = form['teachers'].value as string;
            value.IdTeacher = parseInt(teacher.split(' ')[0]);
            let teacherBkp = form['teachersBackup'].value as string;
            value.IdTeacherBackup = parseInt(teacherBkp.split(' ')[0]);

            //chiamata per creazione lezione a database
            $.ajax({
                type: "POST",
                dataType: 'text',
                url: URLHelper.getMainWebApiUrl() + '/api/Lessons/CreateLesson',
                data: value,
                crossDomain: false,
                xhrFields: {
                    withCredentials: false
                },
                success: function (data) {
                    //reset campi del form
                    form['courses'].value = null;
                    form['modules'].value = null;
                    form['description'].value = null;
                    form['date'].value = null;
                    form['room'].value = null;
                    form['teachers'].value = null;
                    form['teachersBackup'].value = null;
                    ToastHelper.Toast(ToastTypes.Success, "Nuova lezione creata correttamente, ricaricare la pagina per visualizzarla");
                },
                error: function (xhr, textStatus) {
                    ErrorHelper.Handle(xhr, textStatus);
                }
            });
        }
    });

    //chiamata per recuperare tutte le lezioni
    $.ajax({
        type: "GET",
        dataType: 'text',
        url: URLHelper.getMainWebApiUrl() + 'api/Lessons/GetAll',
        crossDomain: false,
        xhrFields: {
            withCredentials: false
        },
        success: function (data) {
            //aggiunta delle lezioni all'interno della tabella
            let json: Array<Lezione> = $.parseJSON(data);
            var options = { year: 'numeric', month: 'numeric', day: 'numeric' };
            for (let r of json) {
                var start = new Date(<any>r.Date);
                $('#tableBody').append('<tr>'
                    + '<td><a href="DettaglioLezione.html?id=' + r.Id.toString() + '">' + r.Id.toString() + '</a></td>'
                    + '<td><a href="DettaglioModulo.html?id=' + r.IdModule.toString() + '">' + r.IdModule + '</a></td>'
                    + '<td><a href="DettaglioPercorso.html?id=' + r.IdCourse.toString() + '">' + r.IdCourse + '</a></td>'
                    + '<td>' + r.Description + '</td>'
                    + '<td>' + start.toLocaleDateString('it-IT', options) + '</td>'
                    + '<td>' + r.Room + '</td>'
                    + '<td><a href="DettaglioRisorsa.html?id=' + r.IdTeacher.toString() + '">' + r.IdTeacher + '</a></td>'
                    + '<td><a href="DettaglioRisorsa.html?id=' + r.IdTeacherBackup.toString() + '">' + r.IdTeacherBackup + '</a></td>'
                    + '</tr>');
            }
        },
        error: function (xhr, textStatus) {
            ErrorHelper.Handle(xhr, textStatus);
        }
    });

    //chiamata per recuperare i percorsi da inserire all'interno dell'oggetto datalist
    $.ajax({
        type: "GET",
        dataType: 'text',
        url: URLHelper.getMainWebApiUrl() + 'api/Courses/GetAll',
        crossDomain: false,
        xhrFields: {
            withCredentials: false
        },
        success: function (data) {
            //aggiunta dei singoli percorsi al campo option della datalist
            let json: Array<Percorso> = $.parseJSON(data);
            for (let r of json) {
                $('#courses').append('<option value="' + r.Id + ' - ' + r.Description + '">');
            }
        },
        error: function (xhr, textStatus) {
            ErrorHelper.Handle(xhr, textStatus);
        }
    });

    //chiamata per recuperare i moduli da inserire all'interno dell'oggetto datalist
    $.ajax({
        type: "GET",
        dataType: 'text',
        url: URLHelper.getMainWebApiUrl() + 'api/Modules/GetAll',
        crossDomain: false,
        xhrFields: {
            withCredentials: false
        },
        success: function (data) {
            //aggiunta dei singoli moduli al campo option della datalist
            let json: Array<Modulo> = $.parseJSON(data);
            for (let r of json) {
                $('#modules').append('<option value="' + r.Id + ' - ' + r.Name + '">');
            }
        },
        error: function (xhr, textStatus) {
            ErrorHelper.Handle(xhr, textStatus);
        }
    });

    //chiamata per recuperare le risorse da inserire all'interno delle due datalist
    $.ajax({
        type: "GET",
        dataType: 'text',
        url: URLHelper.getMainWebApiUrl() + 'api/Resources/GetAll',
        crossDomain: false,
        xhrFields: {
            withCredentials: false
        },
        success: function (data) {
            //aggiunta delle singole risorse al campo option delle due datalist
            let json: Array<Risorsa> = $.parseJSON(data);
            for (let r of json) {
                $('#teachers').append('<option value="' + r.Id + ' - ' + r.Surname + ' ' + r.Name + '">');
                $('#teachersBackup').append('<option value="' + r.Id + ' - ' + r.Surname + ' ' + r.Name + '">');
            }
        },
        error: function (xhr, textStatus) {
            ErrorHelper.Handle(xhr, textStatus);
        }
    });
});

//funzione di ricerca valori all'interno della tabella delle lezioni
$("#filterText").keyup(function () {
    var rows = $("#tableBody").find("tr").hide();
    let val = $("#filterText").val() as string;
    if (val != "") {
        var data = val.split(" ");
        $.each(data, function (i, v) {
            if (v != "") {
                rows.filter(":contains('" + v + "')").show();
            }
        });
    }
    else {
        rows.show();
    }
});