import { Risorsa } from "../DTO/Risorsa";
import { URLHelper } from "../Helper/URLHelper";
import { ToastHelper } from "../Helper/ToastHelper";
import { ToastTypes } from "../Enums";
import { ErrorHelper } from "../Helper/ErrorHelper";

$(document).ready(function () {

    let queryParamId = URLHelper.getUrlParameter("id");

    //chiamata per recupero informazioni dettaglio risorsa
    $.ajax({
        type: "GET",
        dataType: 'text',
        url: URLHelper.getMainWebApiUrl() + 'api/Resources/GetById/' + queryParamId,
        crossDomain: false,
        xhrFields: {
            withCredentials: false
        },
        success: function (data) {
            let res: Risorsa = $.parseJSON(data);
            $('#resourceId').append(res.Id.toString());
            $('#resourceUsername').append(res.Username);
            $('#resourceSurname').append(res.Surname);
            $('#resourceName').append(res.Name);
            if(res.IdStatus){
                $('#resourceIdStatus').append("Disponibile");
            }
            else{
                $('#resourceIdStatus').append("Non disponibile");
            }
        },
        error: function (xhr, textStatus) {
            ErrorHelper.Handle(xhr, textStatus);
        }
    });
});