"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var URLHelper_1 = require("../Helper/URLHelper");
var ErrorHelper_1 = require("../Helper/ErrorHelper");
$(document).ready(function () {
    var queryParamId = URLHelper_1.URLHelper.getUrlParameter("id");
    //chiamata per recupero informazioni dettaglio modulo
    $.ajax({
        type: "GET",
        dataType: 'text',
        url: URLHelper_1.URLHelper.getMainWebApiUrl() + 'api/Modules/GetById/' + queryParamId,
        crossDomain: false,
        xhrFields: {
            withCredentials: false
        },
        success: function (data) {
            var res = $.parseJSON(data);
            $('#moduleId').append(res.Id.toString());
            $('#moduleName').append(res.Name);
            $('#moduleCredits').append(res.Credits.toString());
        },
        error: function (xhr, textStatus) {
            ErrorHelper_1.ErrorHelper.Handle(xhr, textStatus);
        }
    });
});
