"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Risorsa_1 = require("../DTO/Risorsa");
var URLHelper_1 = require("../Helper/URLHelper");
var ToastHelper_1 = require("../Helper/ToastHelper");
var Enums_1 = require("../Enums");
var ErrorHelper_1 = require("../Helper/ErrorHelper");
$(document).ready(function () {
    //inizializzazione form creazione risorsa
    $("#form").validate({
        rules: {
            id: {
                required: true
            },
            username: {
                required: true
            },
            surname: {
                required: true
            },
            name: {
                required: true
            }
        },
        messages: {
            id: "Campo obbligatorio",
            username: "Campo obbligatorio",
            surname: "Campo obbligatorio",
            name: "Campo obbligatorio"
        },
        submitHandler: function (form) {
            //recupero informazioni inserite nel form
            var value = new Risorsa_1.Risorsa();
            value.Id = form['id'].value;
            value.Username = form['username'].value;
            value.Surname = form['surname'].value;
            value.Name = form['name'].value;
            value.IdStatus = +form['idStatus'].checked;
            //chiamata per creazione risorsa a database
            $.ajax({
                type: "POST",
                dataType: 'text',
                url: URLHelper_1.URLHelper.getMainWebApiUrl() + 'api/Resources/CreateResource',
                data: value,
                crossDomain: false,
                xhrFields: {
                    withCredentials: false
                },
                success: function (data) {
                    //reset campi del form
                    form['id'].value = null;
                    form['username'].value = null;
                    form['surname'].value = null;
                    form['name'].value = null;
                    form['idStatus'].value = null;
                    ToastHelper_1.ToastHelper.Toast(Enums_1.ToastTypes.Success, "Nuova risorsa creata correttamente, ricaricare la pagina per visualizzarla");
                },
                error: function (xhr, textStatus) {
                    ErrorHelper_1.ErrorHelper.Handle(xhr, textStatus);
                }
            });
        }
    });
    //chiamata per recuperare tutte le risorse
    $.ajax({
        type: "GET",
        dataType: 'text',
        url: URLHelper_1.URLHelper.getMainWebApiUrl() + 'api/Resources/GetAll',
        crossDomain: false,
        xhrFields: {
            withCredentials: false
        },
        success: function (data) {
            //aggiunta delle risorse all'interno della tabella
            var json = $.parseJSON(data);
            for (var _i = 0, json_1 = json; _i < json_1.length; _i++) {
                var r = json_1[_i];
                var valid = void 0;
                if (r.IdStatus) {
                    valid = "Disponibile";
                }
                else {
                    valid = "Non disponibile";
                }
                $('#tableBody').append('<tr>'
                    + '<td><a href="DettaglioRisorsa.html?id=' + r.Id.toString() + '">' + r.Id.toString() + '</a></td>'
                    + '<td>' + r.Username + '</td>'
                    + '<td>' + r.Surname + '</td>'
                    + '<td>' + r.Name + '</td>'
                    + '<td>' + valid + '</td>'
                    + '</tr>');
            }
        },
        error: function (xhr, textStatus) {
            ErrorHelper_1.ErrorHelper.Handle(xhr, textStatus);
        }
    });
});
//funzione di ricerca valori all'interno della tabella delle risorse
$("#filterText").keyup(function () {
    var rows = $("#tableBody").find("tr").hide();
    var val = $("#filterText").val();
    if (val != "") {
        var data = val.split(" ");
        $.each(data, function (i, v) {
            if (v != "") {
                rows.filter(":contains('" + v + "')").show();
            }
        });
    }
    else {
        rows.show();
    }
});
