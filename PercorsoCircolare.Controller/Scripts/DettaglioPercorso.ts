import { Percorso } from "../DTO/Percorso";
import { URLHelper } from "../Helper/URLHelper";
import { ToastTypes } from "../Enums";
import { ToastHelper } from "../Helper/ToastHelper";
import { ErrorHelper } from "../Helper/ErrorHelper";

$(document).ready(function () {

    let queryParamId = URLHelper.getUrlParameter("id");

    //chiamata per recupero informazioni dettaglio percorso
    $.ajax({
        type: "GET",
        dataType: 'text',
        url: URLHelper.getMainWebApiUrl() + 'api/Courses/GetById/' + queryParamId,
        crossDomain: false,
        xhrFields: {
            withCredentials: false
        },
        success: function (data) {
            var options = { year: 'numeric', month: 'numeric', day: 'numeric' };
            let res: Percorso = $.parseJSON(data);
            var start = new Date(<any>res.StartDate);
            var end = new Date(<any>res.EndDate);
            $('#courseId').append(res.Id.toString());
            $('#courseDescription').append(res.Description.toString());
            $('#courseYear').append(res.Year.toString());
            $('#courseStartDate').append(start.toLocaleDateString('it-IT', options));
            $('#courseEndDate').append(end.toLocaleDateString('it-IT', options));
            $('#courseIdResourceCoordinator').append('<a href="DettaglioRisorsa.html?id=' +  res.IdResourceCoordinator + '">' + res.IdResourceCoordinator.toString() + '</a>');
        },
        error: function (xhr, textStatus) {
            ErrorHelper.Handle(xhr, textStatus);
        }
    });
});