"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Modulo_1 = require("../DTO/Modulo");
var URLHelper_1 = require("../Helper/URLHelper");
var ToastHelper_1 = require("../Helper/ToastHelper");
var Enums_1 = require("../Enums");
var ErrorHelper_1 = require("../Helper/ErrorHelper");
$(document).ready(function () {
    //inizializzazione form creazione modulo
    $("#form").validate({
        rules: {
            name: {
                required: true
            }
        },
        messages: {
            name: "Campo obbligatorio"
        },
        submitHandler: function (form) {
            //recupero informazioni inserite nel form
            var value = new Modulo_1.Modulo();
            value.Name = form['name'].value;
            value.Credits = 0;
            //chiamata per creazione modulo a database
            $.ajax({
                type: "POST",
                dataType: 'text',
                url: URLHelper_1.URLHelper.getMainWebApiUrl() + 'api/Modules/CreateModule',
                data: value,
                crossDomain: false,
                xhrFields: {
                    withCredentials: false
                },
                success: function (data) {
                    //reset campi del form
                    form['name'].value = null;
                    ToastHelper_1.ToastHelper.Toast(Enums_1.ToastTypes.Success, "Nuovo modulo creato correttamente, ricaricare la pagina per visualizzarlo");
                },
                error: function (xhr, textStatus) {
                    ErrorHelper_1.ErrorHelper.Handle(xhr, textStatus);
                }
            });
        }
    });
    //chiamata per recuperare tutti i moduli
    $.ajax({
        type: "GET",
        dataType: 'text',
        url: URLHelper_1.URLHelper.getMainWebApiUrl() + 'api/Modules/GetAll',
        crossDomain: false,
        xhrFields: {
            withCredentials: false
        },
        success: function (data) {
            //aggiunta dei moduli all'interno della tabella
            var json = $.parseJSON(data);
            for (var _i = 0, json_1 = json; _i < json_1.length; _i++) {
                var r = json_1[_i];
                $('#tableBody').append('<tr>'
                    + '<td><a href="DettaglioModulo.html?id=' + r.Id.toString() + '">' + r.Id.toString() + '</a></td>'
                    + '<td>' + r.Name + '</td>'
                    + '<td>' + r.Credits + '</td>'
                    + '</tr>');
            }
        },
        error: function (xhr, textStatus) {
            ErrorHelper_1.ErrorHelper.Handle(xhr, textStatus);
        }
    });
});
//funzione di ricerca valori all'interno della tabella dei moduli
$("#filterText").keyup(function () {
    var rows = $("#tableBody").find("tr").hide();
    var val = $("#filterText").val();
    if (val != "") {
        var data = val.split(" ");
        $.each(data, function (i, v) {
            if (v != "") {
                rows.filter(":contains('" + v + "')").show();
            }
        });
    }
    else {
        rows.show();
    }
});
