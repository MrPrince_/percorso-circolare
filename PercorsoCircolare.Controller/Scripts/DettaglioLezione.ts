import { Lezione } from "../DTO/Lezione";
import { URLHelper } from "../Helper/URLHelper";
import { ToastHelper } from "../Helper/ToastHelper";
import { ToastTypes } from "../Enums";
import { ErrorHelper } from "../Helper/ErrorHelper";

$(document).ready(function () {

    let queryParamId = URLHelper.getUrlParameter("id");

    //chiamata per recupero informazioni dettaglio lezione
    $.ajax({
        type: "GET",
        dataType: 'text',
        url: URLHelper.getMainWebApiUrl() + 'api/Lessons/GetById/' + queryParamId,
        crossDomain: false,
        xhrFields: {
            withCredentials: false
        },
        success: function (data) {
            var options = { year: 'numeric', month: 'numeric', day: 'numeric' };
            let res: Lezione = $.parseJSON(data);
            var start = new Date(<any>res.Date);
            $('#lessonsId').append(res.Id.toString());
            $('#lessonsIdModule').append('<a href="DettaglioModulo.html?id=' + res.IdModule + '">' + res.IdModule.toString() + '</a>');
            $('#lessonsIdCourse').append('<a href="DettaglioPercorso.html?id=' + res.IdCourse + '">' + res.IdCourse.toString() + '</a>');
            $('#lessonDescription').append(res.Description.toString());
            $('#lessonDate').append(start.toLocaleDateString('it-IT', options));
            $('#lessonRoom').append(res.Room.toString());
            $('#lessonIdTeacher').append('<a href="DettaglioRisorsa.html?id=' +  res.IdTeacher + '">' + res.IdTeacher.toString() + '</a>');
            $('#lessonIdTeacherBackup').append('<a href="DettaglioRisorsa.html?id=' +  res.IdTeacherBackup + '">' + res.IdTeacherBackup.toString() + '</a>');
        },
        error: function (xhr, textStatus) {
            ErrorHelper.Handle(xhr, textStatus);
        }
    });
});

//chiamata per l'eliminazione della lezione dal database
$("#deleteBtn").click(function() {
    let queryParamId = URLHelper.getUrlParameter("id");

    $.ajax({
        type: "DELETE",
        dataType: 'text',
        url: URLHelper.getMainWebApiUrl() + 'api/Lessons/DeleteById/' + queryParamId,
        crossDomain: false,
        xhrFields: {
            withCredentials: false
        },
        success: function (data: number) {
            console.log("E' stata eliminata la lezione con id " + queryParamId as string);
            ToastHelper.Toast(ToastTypes.Success, "La lezione con id " + queryParamId + " e' stata eliminata");
         },
        error: function (xhr, textStatus) {
            ErrorHelper.Handle(xhr, textStatus);
        }
    });
  });