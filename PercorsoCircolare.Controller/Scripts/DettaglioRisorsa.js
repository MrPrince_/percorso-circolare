"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var URLHelper_1 = require("../Helper/URLHelper");
var ErrorHelper_1 = require("../Helper/ErrorHelper");
$(document).ready(function () {
    var queryParamId = URLHelper_1.URLHelper.getUrlParameter("id");
    //chiamata per recupero informazioni dettaglio risorsa
    $.ajax({
        type: "GET",
        dataType: 'text',
        url: URLHelper_1.URLHelper.getMainWebApiUrl() + 'api/Resources/GetById/' + queryParamId,
        crossDomain: false,
        xhrFields: {
            withCredentials: false
        },
        success: function (data) {
            var res = $.parseJSON(data);
            $('#resourceId').append(res.Id.toString());
            $('#resourceUsername').append(res.Username);
            $('#resourceSurname').append(res.Surname);
            $('#resourceName').append(res.Name);
            if (res.IdStatus) {
                $('#resourceIdStatus').append("Disponibile");
            }
            else {
                $('#resourceIdStatus').append("Non disponibile");
            }
        },
        error: function (xhr, textStatus) {
            ErrorHelper_1.ErrorHelper.Handle(xhr, textStatus);
        }
    });
});
