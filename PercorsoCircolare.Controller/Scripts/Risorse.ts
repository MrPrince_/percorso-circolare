import { Risorsa } from "../DTO/Risorsa";
import { URLHelper } from "../Helper/URLHelper";
import { ToastHelper } from "../Helper/ToastHelper";
import { ToastTypes } from "../Enums";
import { ErrorHelper } from "../Helper/ErrorHelper";

$(document).ready(function () {

    //inizializzazione form creazione risorsa
    (<any>$("#form")).validate({
        rules: {
            id: {
                required: true
            },
            username: {
                required: true
            },
            surname: {
                required: true
            },
            name: {
                required: true
            }
        },
        messages: {
            id: "Campo obbligatorio",
            username: "Campo obbligatorio",
            surname: "Campo obbligatorio",
            name: "Campo obbligatorio"
        },
        submitHandler: function (form) {
            //recupero informazioni inserite nel form
            var value = new Risorsa();
            value.Id = form['id'].value as number;
            value.Username = form['username'].value as string;
            value.Surname = form['surname'].value as string;
            value.Name = form['name'].value as string;
            value.IdStatus = + form['idStatus'].checked as number;

            //chiamata per creazione risorsa a database
            $.ajax({
                type: "POST",
                dataType: 'text',
                url: URLHelper.getMainWebApiUrl() + 'api/Resources/CreateResource',
                data: value,
                crossDomain: false,
                xhrFields: {
                    withCredentials: false
                },
                success: function (data) {
                    //reset campi del form
                    form['id'].value = null;
                    form['username'].value = null;
                    form['surname'].value = null;
                    form['name'].value = null;
                    form['idStatus'].value = null;
                    ToastHelper.Toast(ToastTypes.Success, "Nuova risorsa creata correttamente, ricaricare la pagina per visualizzarla");
                },
                error: function (xhr, textStatus) {
                    ErrorHelper.Handle(xhr, textStatus);
                }
            });
        }
    });

    //chiamata per recuperare tutte le risorse
    $.ajax({
        type: "GET",
        dataType: 'text',
        url: URLHelper.getMainWebApiUrl() + 'api/Resources/GetAll',
        crossDomain: false,
        xhrFields: {
            withCredentials: false
        },
        success: function (data) {
            //aggiunta delle risorse all'interno della tabella
            let json: Array<Risorsa> = $.parseJSON(data);
            for (let r of json) {
                let valid: string;
                if (r.IdStatus) {
                    valid = "Disponibile";
                }
                else {
                    valid = "Non disponibile";
                }
                $('#tableBody').append('<tr>'
                    + '<td><a href="DettaglioRisorsa.html?id=' + r.Id.toString() + '">' + r.Id.toString() + '</a></td>'
                    + '<td>' + r.Username + '</td>'
                    + '<td>' + r.Surname + '</td>'
                    + '<td>' + r.Name + '</td>'
                    + '<td>' + valid + '</td>'
                    + '</tr>');
            }
        },
        error: function (xhr, textStatus) {
            ErrorHelper.Handle(xhr, textStatus);
        }
    });
});

//funzione di ricerca valori all'interno della tabella delle risorse
$("#filterText").keyup(function () {
    var rows = $("#tableBody").find("tr").hide();
    let val = $("#filterText").val() as string;
    if (val != "") {
        var data = val.split(" ");
        $.each(data, function (i, v) {
            if(v != ""){
                rows.filter(":contains('" + v + "')").show();
            }
        });
    }
    else {
        rows.show();
    }
});