"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var URLHelper_1 = require("../Helper/URLHelper");
var ToastHelper_1 = require("../Helper/ToastHelper");
var Enums_1 = require("../Enums");
var ErrorHelper_1 = require("../Helper/ErrorHelper");
$(document).ready(function () {
    var queryParamId = URLHelper_1.URLHelper.getUrlParameter("id");
    //chiamata per recupero informazioni dettaglio lezione
    $.ajax({
        type: "GET",
        dataType: 'text',
        url: URLHelper_1.URLHelper.getMainWebApiUrl() + 'api/Lessons/GetById/' + queryParamId,
        crossDomain: false,
        xhrFields: {
            withCredentials: false
        },
        success: function (data) {
            var options = { year: 'numeric', month: 'numeric', day: 'numeric' };
            var res = $.parseJSON(data);
            var start = new Date(res.Date);
            $('#lessonsId').append(res.Id.toString());
            $('#lessonsIdModule').append('<a href="DettaglioModulo.html?id=' + res.IdModule + '">' + res.IdModule.toString() + '</a>');
            $('#lessonsIdCourse').append('<a href="DettaglioPercorso.html?id=' + res.IdCourse + '">' + res.IdCourse.toString() + '</a>');
            $('#lessonDescription').append(res.Description.toString());
            $('#lessonDate').append(start.toLocaleDateString('it-IT', options));
            $('#lessonRoom').append(res.Room.toString());
            $('#lessonIdTeacher').append('<a href="DettaglioRisorsa.html?id=' + res.IdTeacher + '">' + res.IdTeacher.toString() + '</a>');
            $('#lessonIdTeacherBackup').append('<a href="DettaglioRisorsa.html?id=' + res.IdTeacherBackup + '">' + res.IdTeacherBackup.toString() + '</a>');
        },
        error: function (xhr, textStatus) {
            ErrorHelper_1.ErrorHelper.Handle(xhr, textStatus);
        }
    });
});
//chiamata per l'eliminazione della lezione dal database
$("#deleteBtn").click(function () {
    var queryParamId = URLHelper_1.URLHelper.getUrlParameter("id");
    $.ajax({
        type: "DELETE",
        dataType: 'text',
        url: URLHelper_1.URLHelper.getMainWebApiUrl() + 'api/Lessons/DeleteById/' + queryParamId,
        crossDomain: false,
        xhrFields: {
            withCredentials: false
        },
        success: function (data) {
            console.log("E' stata eliminata la lezione con id " + queryParamId);
            ToastHelper_1.ToastHelper.Toast(Enums_1.ToastTypes.Success, "La lezione con id " + queryParamId + " e' stata eliminata");
        },
        error: function (xhr, textStatus) {
            ErrorHelper_1.ErrorHelper.Handle(xhr, textStatus);
        }
    });
});
