"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Percorso_1 = require("../DTO/Percorso");
var URLHelper_1 = require("../Helper/URLHelper");
var ToastHelper_1 = require("../Helper/ToastHelper");
var Enums_1 = require("../Enums");
var ErrorHelper_1 = require("../Helper/ErrorHelper");
$(document).ready(function () {
    //inizializzazione form creazione percorso
    $("#form").validate({
        rules: {
            description: {
                required: true
            },
            year: {
                required: true
            },
            startDate: {
                required: true
            },
            endDate: {
                required: true
            },
            coordinators: {
                required: true
            }
        },
        messages: {
            description: "Campo obbligatorio",
            year: "Campo obbligatorio",
            startDate: "Campo obbligatorio",
            endDate: "Campo obbligatorio",
            coordinators: "Campo obbligatorio"
        },
        submitHandler: function (form) {
            //recupero informazioni inserite nel form
            var value = new Percorso_1.Percorso();
            value.Description = form['description'].value;
            value.Year = form['year'].value;
            value.StartDate = form['startDate'].value;
            value.EndDate = form['endDate'].value;
            var x = form['coordinators'].value;
            value.IdResourceCoordinator = parseInt(x.split(' ')[0]);
            //chiamata per creazione percorso a database
            $.ajax({
                type: "POST",
                dataType: 'text',
                url: URLHelper_1.URLHelper.getMainWebApiUrl() + 'api/Courses/CreateCourse',
                data: value,
                crossDomain: false,
                xhrFields: {
                    withCredentials: false
                },
                success: function (data) {
                    //reset campi del form
                    form['description'].value = null;
                    form['year'].value = null;
                    form['startDate'].value = null;
                    form['endDate'].value = null;
                    form['coordinators'].value = null;
                    ToastHelper_1.ToastHelper.Toast(Enums_1.ToastTypes.Success, "Nuovo percorso creato correttamente, ricaricare la pagina per visualizzarlo");
                },
                error: function (xhr, textStatus) {
                    ErrorHelper_1.ErrorHelper.Handle(xhr, textStatus);
                }
            });
        }
    });
    //chiamata per recuperare tutti i percorsi
    $.ajax({
        type: "GET",
        dataType: 'text',
        url: URLHelper_1.URLHelper.getMainWebApiUrl() + 'api/Courses/GetAll',
        crossDomain: false,
        xhrFields: {
            withCredentials: false
        },
        success: function (data) {
            //aggiunta dei percorsi all'interno della tabella
            var json = $.parseJSON(data);
            var options = { year: 'numeric', month: 'numeric', day: 'numeric' };
            for (var _i = 0, json_1 = json; _i < json_1.length; _i++) {
                var r = json_1[_i];
                var start = new Date(r.StartDate);
                var end = new Date(r.EndDate);
                $('#tableBody').append('<tr>'
                    + '<td><a href="DettaglioPercorso.html?id=' + r.Id.toString() + '">' + r.Id.toString() + '</a></td>'
                    + '<td>' + r.Description + '</td>'
                    + '<td>' + r.Year + '</td>'
                    + '<td>' + start.toLocaleDateString('it-IT', options) + '</td>'
                    + '<td>' + end.toLocaleDateString('it-IT', options) + '</td>'
                    + '</tr>');
            }
        },
        error: function (xhr, textStatus) {
            ErrorHelper_1.ErrorHelper.Handle(xhr, textStatus);
        }
    });
    //chiamata per recuperare le risorsa da inserire all'interno dell'oggetto datalist
    $.ajax({
        type: "GET",
        dataType: 'text',
        url: URLHelper_1.URLHelper.getMainWebApiUrl() + 'api/Resources/GetAll',
        crossDomain: false,
        xhrFields: {
            withCredentials: false
        },
        success: function (data) {
            //aggiunta delle singole risorse al campo option della datalist
            var json = $.parseJSON(data);
            for (var _i = 0, json_2 = json; _i < json_2.length; _i++) {
                var r = json_2[_i];
                $('#coordinators').append('<option value="' + r.Id + ' - ' + r.Surname + ' ' + r.Name + '">');
            }
        },
        error: function (xhr, textStatus) {
            ErrorHelper_1.ErrorHelper.Handle(xhr, textStatus);
        }
    });
});
//funzione di ricerca valori all'interno della tabella dei percorsi
$("#filterText").keyup(function () {
    var rows = $("#tableBody").find("tr").hide();
    var val = $("#filterText").val();
    if (val != "") {
        var data = val.split(" ");
        $.each(data, function (i, v) {
            if (v != "") {
                rows.filter(":contains('" + v + "')").show();
            }
        });
    }
    else {
        rows.show();
    }
});
