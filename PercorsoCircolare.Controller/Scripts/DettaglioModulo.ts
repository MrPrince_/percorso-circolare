import { Modulo } from "../DTO/Modulo";
import { URLHelper } from "../Helper/URLHelper";
import { ToastHelper } from "../Helper/ToastHelper";
import { ToastTypes } from "../Enums";
import { ErrorHelper } from "../Helper/ErrorHelper";

$(document).ready(function () {

    let queryParamId = URLHelper.getUrlParameter("id");

    //chiamata per recupero informazioni dettaglio modulo
    $.ajax({
        type: "GET",
        dataType: 'text',
        url: URLHelper.getMainWebApiUrl() + 'api/Modules/GetById/' + queryParamId,
        crossDomain: false,
        xhrFields: {
            withCredentials: false
        },
        success: function (data) {
            let res: Modulo = $.parseJSON(data);
            $('#moduleId').append(res.Id.toString());
            $('#moduleName').append(res.Name);
            $('#moduleCredits').append(res.Credits.toString());
        },
        error: function (xhr, textStatus) {
            ErrorHelper.Handle(xhr, textStatus);
        }
    });
});