"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Lezione_1 = require("../DTO/Lezione");
var URLHelper_1 = require("../Helper/URLHelper");
var ToastHelper_1 = require("../Helper/ToastHelper");
var Enums_1 = require("../Enums");
var ErrorHelper_1 = require("../Helper/ErrorHelper");
$(document).ready(function () {
    //inizializzazione form creazione lezione
    $("#form").validate({
        rules: {
            courses: {
                required: true
            },
            modules: {
                required: true
            },
            description: {
                required: true
            },
            date: {
                required: true
            },
            room: {
                required: true
            },
            teachers: {
                required: true
            },
            teachersBackup: {
                required: true
            },
        },
        messages: {
            courses: "Campo obbligatorio",
            modules: "Campo obbligatorio",
            description: "Campo obbligatorio",
            date: "Campo obbligatorio",
            room: "Campo obbligatorio",
            teachers: "Campo obbligatorio",
            teachersBackup: "Campo obbligatorio"
        },
        submitHandler: function (form) {
            //recupero informazioni inserite nel form
            var value = new Lezione_1.Lezione();
            var course = form['courses'].value;
            value.IdCourse = parseInt(course.split(' ')[0]);
            var mod = form['modules'].value;
            value.IdModule = parseInt(mod.split(' ')[0]);
            value.Description = form['description'].value;
            value.Date = form['date'].value;
            value.Room = form['room'].value;
            var teacher = form['teachers'].value;
            value.IdTeacher = parseInt(teacher.split(' ')[0]);
            var teacherBkp = form['teachersBackup'].value;
            value.IdTeacherBackup = parseInt(teacherBkp.split(' ')[0]);
            //chiamata per creazione lezione a database
            $.ajax({
                type: "POST",
                dataType: 'text',
                url: URLHelper_1.URLHelper.getMainWebApiUrl() + '/api/Lessons/CreateLesson',
                data: value,
                crossDomain: false,
                xhrFields: {
                    withCredentials: false
                },
                success: function (data) {
                    //reset campi del form
                    form['courses'].value = null;
                    form['modules'].value = null;
                    form['description'].value = null;
                    form['date'].value = null;
                    form['room'].value = null;
                    form['teachers'].value = null;
                    form['teachersBackup'].value = null;
                    ToastHelper_1.ToastHelper.Toast(Enums_1.ToastTypes.Success, "Nuova lezione creata correttamente, ricaricare la pagina per visualizzarla");
                },
                error: function (xhr, textStatus) {
                    ErrorHelper_1.ErrorHelper.Handle(xhr, textStatus);
                }
            });
        }
    });
    //chiamata per recuperare tutte le lezioni
    $.ajax({
        type: "GET",
        dataType: 'text',
        url: URLHelper_1.URLHelper.getMainWebApiUrl() + 'api/Lessons/GetAll',
        crossDomain: false,
        xhrFields: {
            withCredentials: false
        },
        success: function (data) {
            //aggiunta delle lezioni all'interno della tabella
            var json = $.parseJSON(data);
            var options = { year: 'numeric', month: 'numeric', day: 'numeric' };
            for (var _i = 0, json_1 = json; _i < json_1.length; _i++) {
                var r = json_1[_i];
                var start = new Date(r.Date);
                $('#tableBody').append('<tr>'
                    + '<td><a href="DettaglioLezione.html?id=' + r.Id.toString() + '">' + r.Id.toString() + '</a></td>'
                    + '<td><a href="DettaglioModulo.html?id=' + r.IdModule.toString() + '">' + r.IdModule + '</a></td>'
                    + '<td><a href="DettaglioPercorso.html?id=' + r.IdCourse.toString() + '">' + r.IdCourse + '</a></td>'
                    + '<td>' + r.Description + '</td>'
                    + '<td>' + start.toLocaleDateString('it-IT', options) + '</td>'
                    + '<td>' + r.Room + '</td>'
                    + '<td><a href="DettaglioRisorsa.html?id=' + r.IdTeacher.toString() + '">' + r.IdTeacher + '</a></td>'
                    + '<td><a href="DettaglioRisorsa.html?id=' + r.IdTeacherBackup.toString() + '">' + r.IdTeacherBackup + '</a></td>'
                    + '</tr>');
            }
        },
        error: function (xhr, textStatus) {
            ErrorHelper_1.ErrorHelper.Handle(xhr, textStatus);
        }
    });
    //chiamata per recuperare i percorsi da inserire all'interno dell'oggetto datalist
    $.ajax({
        type: "GET",
        dataType: 'text',
        url: URLHelper_1.URLHelper.getMainWebApiUrl() + 'api/Courses/GetAll',
        crossDomain: false,
        xhrFields: {
            withCredentials: false
        },
        success: function (data) {
            //aggiunta dei singoli percorsi al campo option della datalist
            var json = $.parseJSON(data);
            for (var _i = 0, json_2 = json; _i < json_2.length; _i++) {
                var r = json_2[_i];
                $('#courses').append('<option value="' + r.Id + ' - ' + r.Description + '">');
            }
        },
        error: function (xhr, textStatus) {
            ErrorHelper_1.ErrorHelper.Handle(xhr, textStatus);
        }
    });
    //chiamata per recuperare i moduli da inserire all'interno dell'oggetto datalist
    $.ajax({
        type: "GET",
        dataType: 'text',
        url: URLHelper_1.URLHelper.getMainWebApiUrl() + 'api/Modules/GetAll',
        crossDomain: false,
        xhrFields: {
            withCredentials: false
        },
        success: function (data) {
            //aggiunta dei singoli moduli al campo option della datalist
            var json = $.parseJSON(data);
            for (var _i = 0, json_3 = json; _i < json_3.length; _i++) {
                var r = json_3[_i];
                $('#modules').append('<option value="' + r.Id + ' - ' + r.Name + '">');
            }
        },
        error: function (xhr, textStatus) {
            ErrorHelper_1.ErrorHelper.Handle(xhr, textStatus);
        }
    });
    //chiamata per recuperare le risorse da inserire all'interno delle due datalist
    $.ajax({
        type: "GET",
        dataType: 'text',
        url: URLHelper_1.URLHelper.getMainWebApiUrl() + 'api/Resources/GetAll',
        crossDomain: false,
        xhrFields: {
            withCredentials: false
        },
        success: function (data) {
            //aggiunta delle singole risorse al campo option delle due datalist
            var json = $.parseJSON(data);
            for (var _i = 0, json_4 = json; _i < json_4.length; _i++) {
                var r = json_4[_i];
                $('#teachers').append('<option value="' + r.Id + ' - ' + r.Surname + ' ' + r.Name + '">');
                $('#teachersBackup').append('<option value="' + r.Id + ' - ' + r.Surname + ' ' + r.Name + '">');
            }
        },
        error: function (xhr, textStatus) {
            ErrorHelper_1.ErrorHelper.Handle(xhr, textStatus);
        }
    });
});
//funzione di ricerca valori all'interno della tabella delle lezioni
$("#filterText").keyup(function () {
    var rows = $("#tableBody").find("tr").hide();
    var val = $("#filterText").val();
    if (val != "") {
        var data = val.split(" ");
        $.each(data, function (i, v) {
            if (v != "") {
                rows.filter(":contains('" + v + "')").show();
            }
        });
    }
    else {
        rows.show();
    }
});
