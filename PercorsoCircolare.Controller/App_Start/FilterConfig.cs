﻿using System.Web;
using System.Web.Mvc;

namespace PercorsoCircolare.Controller
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
