"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ToastTypes;
(function (ToastTypes) {
    ToastTypes["Success"] = "success";
    ToastTypes["Info"] = "info";
    ToastTypes["Warning"] = "warning";
    ToastTypes["Error"] = "error";
})(ToastTypes = exports.ToastTypes || (exports.ToastTypes = {}));
