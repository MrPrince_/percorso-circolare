﻿using PercorsoCircolare.DTO;
using PercorsoCircolare.Manager;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace PercorsoCircolare.Controller.Controllers
{
    [RoutePrefix("api/Modules")]
    public class ModulesController : ApiController
    {
        [HttpGet]
        [Route("GetAll")]
        public IHttpActionResult GetAll()
        {
            try
            {
                List<ModuleDTO> modules = ModulesManager.GetAll();
                return Ok(modules);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet]
        [Route("GetById/{Id}")]
        public IHttpActionResult GetById(int id)
        {
            try
            {
                ModuleDTO module = ModulesManager.GetById(id);
                return Ok(module);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpPost]
        [Route("CreateModule")]
        public IHttpActionResult CreateModule([FromBody]ModuleDTO module)
        {
            try
            {
                ModulesManager.CreateModule(module);
                return Ok(module);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
    }
}