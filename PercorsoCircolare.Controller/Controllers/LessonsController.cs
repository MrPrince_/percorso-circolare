﻿using PercorsoCircolare.DTO;
using PercorsoCircolare.Manager;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace PercorsoCircolare.Controller.Controllers
{
    [RoutePrefix("api/Lessons")]
    public class LessonsController : ApiController
    {
        [HttpGet]
        [Route("GetAll")]
        public IHttpActionResult GetAll()
        {
            try
            {
                List<LessonDTO> lessons = LessonsManager.GetAll();
                return Ok(lessons);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet]
        [Route("GetById/{Id}")]
        public IHttpActionResult GetById(int id)
        {
            try
            {
                LessonDTO lesson = LessonsManager.GetById(id);
                return Ok(lesson);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpPost]
        [Route("CreateLesson")]
        public IHttpActionResult CreateLesson([FromBody]LessonDTO lesson)
        {
            try
            {
                LessonsManager.CreateLesson(lesson);
                return Ok(lesson);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpDelete]
        [Route("DeleteById/{Id}")]
        public IHttpActionResult DeleteById(int id)
        {
            try
            {
                LessonsManager.DeleteById(id);
                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
    }
}