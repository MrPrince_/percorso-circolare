﻿using PercorsoCircolare.DTO;
using PercorsoCircolare.Manager;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace PercorsoCircolare.Controller.Controllers
{
    [RoutePrefix("api/Courses")]
    public class CoursesController : ApiController
    {
        [HttpGet]
        [Route("GetAll")]
        public IHttpActionResult GetAll()
        {
            try
            {
                List<CourseDTO> courses = CoursesManager.GetAll();
                return Ok(courses);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet]
        [Route("GetById/{Id}")]
        public IHttpActionResult GetById(int id)
        {
            try
            {
                CourseDTO course = CoursesManager.GetById(id);
                return Ok(course);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpPost]
        [Route("CreateCourse")]
        public IHttpActionResult CreateCourse([FromBody]CourseDTO course)
        {
            try
            {
                CoursesManager.CreateCourse(course);
                return Ok(course);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
    }
}