﻿using PercorsoCircolare.DTO;
using PercorsoCircolare.Manager;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace PercorsoCircolare.Controller.Controllers
{
    [RoutePrefix("api/Resources")]
    public class ResourcesController : ApiController
    {
        [HttpGet]
        [Route("GetAll")]
        public IHttpActionResult GetAll()
        {
            try
            {
                List<ResourceDTO> resources = ResourcesManager.GetAll();
                return Ok(resources);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet]
        [Route("GetById/{Id}")]
        public IHttpActionResult GetById(int id)
        {
            try
            {
                ResourceDTO resource = ResourcesManager.GetById(id);
                return Ok(resource);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpPost]
        [Route("CreateResource")]
        public IHttpActionResult CreateResource([FromBody]ResourceDTO resource)
        {
            try
            {
                ResourcesManager.CreateResource(resource);
                return Ok(resource);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
    }
}