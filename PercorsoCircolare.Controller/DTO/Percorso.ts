export class Percorso {
    Id: number;
    Description: string;
    Year: number;
    StartDate: Date;
    EndDate: Date;
    IdResourceCoordinator: number;
}