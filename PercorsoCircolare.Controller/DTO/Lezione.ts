export class Lezione {
    Id: number;
    IdModule: number;
    IdCourse: number;
    Description: string;
    Date: Date;
    Room: string;
    IdTeacher: number;
    IdTeacherBackup: number;
}