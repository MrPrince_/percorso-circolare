"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var toastr = require("toastr");
var ToastHelper;
(function (ToastHelper) {
    var options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    function Toast(type, msg) {
        toastr.options = options;
        toastr[type](msg);
    }
    ToastHelper.Toast = Toast;
})(ToastHelper = exports.ToastHelper || (exports.ToastHelper = {}));
