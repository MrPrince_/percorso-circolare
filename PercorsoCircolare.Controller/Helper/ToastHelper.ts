import * as toastr from "toastr";
import { ToastTypes } from "../Enums";

export module ToastHelper {

    var options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
    }

    export function Toast(type: ToastTypes, msg: string){
        (<any>toastr).options = options;
        toastr[type](msg);
    }
}