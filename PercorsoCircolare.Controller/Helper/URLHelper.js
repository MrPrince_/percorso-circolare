"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var URLHelper;
(function (URLHelper) {
    var mainUrl = "http://localhost:52879/";
    function getMainWebApiUrl() {
        return mainUrl;
    }
    URLHelper.getMainWebApiUrl = getMainWebApiUrl;
    function getUrlParameter(sParam) {
        var sPageURL = window.location.search.substring(1), sURLVariables = sPageURL.split('&'), sParameterName, i;
        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
            }
        }
    }
    URLHelper.getUrlParameter = getUrlParameter;
})(URLHelper = exports.URLHelper || (exports.URLHelper = {}));
