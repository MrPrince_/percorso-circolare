"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Enums_1 = require("../Enums");
var ToastHelper_1 = require("./ToastHelper");
var ErrorHelper;
(function (ErrorHelper) {
    function Handle(xhr, textStatus) {
        console.log(xhr.responseText);
        console.log(textStatus);
        if (xhr.responseText != null) {
            var err = JSON.parse(xhr.responseText);
            ToastHelper_1.ToastHelper.Toast(Enums_1.ToastTypes.Error, err.ExceptionMessage);
        }
        else {
            ToastHelper_1.ToastHelper.Toast(Enums_1.ToastTypes.Error, "Qualcosa e' andato storto!");
        }
    }
    ErrorHelper.Handle = Handle;
})(ErrorHelper = exports.ErrorHelper || (exports.ErrorHelper = {}));
