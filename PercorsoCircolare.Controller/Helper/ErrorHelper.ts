import { ToastTypes } from "../Enums";
import { ToastHelper } from "./ToastHelper";

export module ErrorHelper {

    export function Handle(xhr, textStatus) {
        console.log(xhr.responseText);
        console.log(textStatus);
        if(xhr.responseText != null){
            var err = JSON.parse(xhr.responseText);
            ToastHelper.Toast(ToastTypes.Error, err.ExceptionMessage);
        }
        else{
            ToastHelper.Toast(ToastTypes.Error, "Qualcosa e' andato storto!");
        }
    }
}