# Setup

## Database

### Creazione
Il file **"db script.txt"** all'interno della root del progetto contiene la query per la creazione del database, e il popolamento delle tabelle con alcuni dati mockati.

### Aggiornare ConnectionString

Sarà necessario modificare la connection string di collegamento al database in:

"PercorsoCircolare.Controller" > "Web.config" > "PercorsoCircolareEntities" (riga 22) - Nel caso di database in locale, è sufficiente sostituire il campo *data source* (attualmente DESKTOP-KC07984\SQLEXPRESS) con l'indirizzo della macchina su cui avete creato il database.

Nel caso si vogliano testare anche i semplici test eseguiti lato server, si dovrà aggiornare anche la connection string in "PercorsoCircolare.Tests" > "App.config" > "PercorsoCircolareEntities" (riga 8).

## Client

E' necessario eseguire un comando di **npm install** all'interno della cartella "PercorsoCircolare.Controller".

Non è necessario re-*buildare* il client, dato che il file corretti sono già presenti nella solution, ma nel caso lo si volesse fare, sarà sufficiente eseguire un comando **npm run build** (il quale lancerà *'tsc'*, per la compilazione del codice typescript, per poi *browserify*-izzare i file appena creati - soluzione adottata per ovviare ai problemi legati ai vari *exports* e *require*, che non sono riuscito ad risolvere tramite il *tsconfig.json* o in altro modo più agevole)

# Avvio

- Impostare il progetto "PercorsoCircolare.Controller" come progetto di avvio;
- Impostare la pagina di avvio in "PercorsoCircolare.Controller" > "Properties" > "Web" > "Specific page" -> "index.html"
- Avviarlo tramite **IISExpress** integrato in Visual Studio